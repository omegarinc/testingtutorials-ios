//
//  CoreDataWork.swift
//  TestingTutorials
//
//  Created by Roman Petrov on 02/03/16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

import Foundation
import MagicalRecord
@testable import TestingTutorials

class CoreDataWork {
    func createUser(name: String, age: Int) -> User? {
        if let user = User.MR_createEntity() {
            user.name = name
            user.age = age
            return user
        } else {
            return nil
        }
    }
    
    func sortedUsers() -> [User] {
        if let foundUsers = User.MR_findAllSortedBy("age", ascending: true) {
            var users: [User] = []
            for obj in foundUsers {
                users.append(obj as! User)
            }
            return users
        } else {
            return []
        }
    }
}
