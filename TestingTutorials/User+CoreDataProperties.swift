//
//  User+CoreDataProperties.swift
//  TestingTutorials
//
//  Created by Roman Petrov on 11/12/15.
//  Copyright © 2015 Omega-R. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

public extension User {

    @NSManaged var name: String?
    @NSManaged var age: NSNumber?

}
