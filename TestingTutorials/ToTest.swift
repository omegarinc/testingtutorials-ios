//
//  ToTest.swift
//  TestingTutorials
//
//  Created by Roman Petrov on 09/12/15.
//  Copyright © 2015 Omega-R. All rights reserved.
//

import Foundation

// MARK: Class with API call

protocol APIProtocol: class {
    func userInfo(completion:(userID: Int, userName: String) -> ())
}

class WithAPICalls {
    var api: APIProtocol? = nil
    
    init(apiObject: APIProtocol) {
        api = apiObject
    }
    
    func userDescription(completion:(description: String) ->()) {
        api!.userInfo { (userID, userName) -> () in
            let description = ("\(userID): \(userName)")
            completion(description: description)
        }
    }
}



// MARK: Class with a delegate

protocol SomeDelegate: class {
    func someDelegateFunction() -> Void
}

class WithDelegate {
    weak var delegate: SomeDelegate?
    
    init(delegateObject: SomeDelegate) {
        delegate = delegateObject
    }
    
    func mayCallDelegate(shouldCall: Bool) -> Void {
        if shouldCall {
            delegate?.someDelegateFunction()
        }
    }
}



