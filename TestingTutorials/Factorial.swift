//
//  Factorial.swift
//  TestingTutorials
//
//  Created by Roman Petrov on 02/03/16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

import Foundation

class Factorial {
    
    func factorialGood(n: Int) -> Int? {
        if n < 0 {
            return nil
        }
        if n == 0 || n == 1 {
            return 1
        }
        
        var result = 1
        for i in 2...n {
            result *= i
        }
        
        return result
    }
    
    func factorialBad(n: Int) -> Int {
        var result = 1
        
        for var i = 1; i <= n; i++ {
            result *= i
        }
        
        return result
    }
    
    func factorialRecursive(n: Int) -> Int? {
        if n < 0 {
            return nil
        }
        if n == 0 || n == 1 {
            return 1
        }
        return n * factorialRecursive(n - 1)!
    }
}
