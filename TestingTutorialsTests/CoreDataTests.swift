//
//  CoreDataTests.swift
//  TestingTutorials
//
//  Created by Roman Petrov on 02/03/16.
//  Copyright © 2016 Omega-R. All rights reserved.
//

import XCTest
import MagicalRecord
@testable import TestingTutorials

class CoreDataTests: XCTestCase {
    func createUser(name: String, age: Int) -> User? {
        if let user = User.MR_createEntity() {
            user.name = name
            user.age = age
            return user
        } else {
            return nil
        }
    }
    
    override func setUp() {
        MagicalRecord.setupCoreDataStackWithInMemoryStore()
        createUser("Ford Prefect", age: 35)
        createUser("Arthur Dent", age: 30)
        createUser("Trillian", age: 29)
    }
    
    override func tearDown() {
        User.MR_truncateAll()
        MagicalRecord.cleanUp()
    }
    
    func testSortedUsers() {
        let obj = CoreDataWork()
        let sorted = obj.sortedUsers()
        let ages = sorted.map { (user) -> Int in
            return Int(user.age!)
        }
        XCTAssert(ages == ages.sort())
    }
}
