//
//  OtherTests.swift
//  TestingTutorials
//
//  Created by Roman Petrov on 09/12/15.
//  Copyright © 2015 Omega-R. All rights reserved.
//

import XCTest
import MagicalRecord
@testable import TestingTutorials

class MockAPI: APIProtocol {
    func userInfo(completion:(userID: Int, userName: String) -> ()) {
        completion(userID: 123, userName: "Test User")
    }
}

class MockDelegate: SomeDelegate {
    var wasCalled = false
    
    func someDelegateFunction() {
        wasCalled = true
    }
}

class OtherTests: XCTestCase {
    
    func testAPI() {
        let mockAPI = MockAPI()
        let obj = WithAPICalls(apiObject: mockAPI)
        var userDescription = ""
        
        let expectation = expectationWithDescription("userDescription")
        
        obj.userDescription { (description) -> () in
            userDescription = description
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(10, handler: nil)

        XCTAssert(userDescription == "123: Test User")
    }
    
    func testDelegateCall() {
        let mockDelegate = MockDelegate()
        let obj = WithDelegate(delegateObject: mockDelegate)
        
        mockDelegate.wasCalled = false
        obj.mayCallDelegate(true)
        XCTAssert(mockDelegate.wasCalled == true)
        
        mockDelegate.wasCalled = false
        obj.mayCallDelegate(false)
        XCTAssert(mockDelegate.wasCalled == false)
    }
}


