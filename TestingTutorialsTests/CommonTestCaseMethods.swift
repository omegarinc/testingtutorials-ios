//
//  CommonTestCaseMethodsWrong.swift
//  TestingTutorials
//
//  Created by Roman Petrov on 09/12/15.
//  Copyright © 2015 Omega-R. All rights reserved.
//

// This is a demo of implementing common methods to be shared between several test cases.

import XCTest

// MARK: A wrong way: subclassing XCTestCase

// It is not recommended to add common methods to XCTestCase by subclassing.
// This approach creates a new test case in the list while it's not a real test case

class ExtendedTestCase: XCTestCase {
    
    override func someCommonMethod() -> Void {
        // ...
    }
}

class CommonMethodsSubclassing: ExtendedTestCase {
    
    func testSomething() {
        self.someCommonMethod()
    }
    
}



// MARK: A right way #1: Extensions

extension XCTestCase {
    func someCommonMethod() -> Void {
        // ...
    }
}

class CommonMethodsExtension: XCTestCase {
    
    func testCommonMethodFromExtension() {
        someCommonMethod()
        XCTAssert(true)
    }
}



// MARK: A right way #2: Protocols

protocol MoreTestCommonMethods {
    func someOtherMethod()
    func yetAnotherCommonMethod()
}

extension MoreTestCommonMethods {
    func someOtherMethod() {
        // ...
    }
    
    func yetAnotherCommonMethod() {
        
    }
}


class CommonMethodsProtocol: XCTestCase, MoreTestCommonMethods {
    
    func testCommonMethodsFromProtocol() {
        someOtherMethod()
        yetAnotherCommonMethod()
        XCTAssert(true)
    }
}
