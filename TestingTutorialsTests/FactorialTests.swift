//
//  FactorialTests.swift
//  TestingTutorials
//
//  Created by Roman Petrov on 09/12/15.
//  Copyright © 2015 Omega-R. All rights reserved.
//

import XCTest

class FactorialTests: XCTestCase {
    
    // MARK: Regular tests for a function

    // Demo of a passing test
    func testFactorialGood() {
        let obj = Factorial()
        
        XCTAssert(obj.factorialGood(1) == 1)
        XCTAssert(obj.factorialGood(5) == 120)
        XCTAssert(obj.factorialGood(0) == 1)
        XCTAssert(obj.factorialGood(-1) == nil)
    }
    
    // Demo of a failing test
    func testFactorialBad() {
        let obj = Factorial()
        
        XCTAssert(obj.factorialBad(1) == 1)
        XCTAssert(obj.factorialBad(5) == 120)
        XCTAssert(obj.factorialBad(0) == 1)
        // As factorialBad always returns a number, let's hope that it will at least return 0 for a negative input
        XCTAssert(obj.factorialBad(-1) == 0)
    }

    
    
    // MARK: Performance tests demonstration
    
    func testPerformanceFactorialGood() {
        let obj = Factorial()

        self.measureBlock {
            for _ in 1...100_000 {
                for i in 1...12 {
                    obj.factorialGood(i)
                }
            }
        }
    }
    
    func testPerformanceFactorialRecursive() {
        let obj = Factorial()
        
        self.measureBlock {
            for _ in 1...100_000 {
                for i in 1...12 {
                    obj.factorialRecursive(i)
                }
            }
        }
    }
}
